FROM centos:7

RUN yum install python3 python3-pip -y

COPY python-api.py python-api.py
COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

CMD ["python3", "python-api.py"]
